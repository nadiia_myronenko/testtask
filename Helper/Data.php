<?php

namespace Task\Test\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    const XML_PATH_TEST = 'categories/';

    public function getConfigValue($code, $group = null, $storeId = null)
    {
        return $this->scopeConfig->getValue(
            $this->getPath($code, $group), ScopeInterface::SCOPE_STORE, $storeId
        );
    }

    public function getPath($code, $group = null)
    {
        $path = $group ? ($group . '/') : 'general/';
        return self::XML_PATH_TEST . $path . $code;
    }
}



