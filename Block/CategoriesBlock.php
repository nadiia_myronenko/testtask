<?php

namespace Task\Test\Block;

use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Framework\View\Element\Template;
use Task\Test\Helper\Data;
use Psr\Log\LoggerInterface;

class CategoriesBlock extends Template
{
    private $titleBlock;

    protected $categoryRepository;

    protected $helperData;

    protected $messageManager;

    protected $_logger;

    public function __construct(
        CategoryRepositoryInterface $categoryRepository,
        Data $helperData,
        Template\Context $context,
        LoggerInterface $logger,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->categoryRepository = $categoryRepository;
        $this->helperData = $helperData;
        $this->_logger = $logger;
    }

    public function getCategories()
    {
        $categories = array();
        try {
            $categoriesIds = explode(',', $this->helperData->getConfigValue('categories_id'));

            foreach ($categoriesIds as $id) {
                $categories[] = $this->categoryRepository->get($id);
            }
        }
        catch (\Exception $e) {
            $this->_logger->critical($e);
        }

        return $categories;
    }

    public function getTitleBlock()
    {
        $this->titleBlock = $this->helperData->getConfigValue('block_title');
        return $this->titleBlock;
    }
}