<?php

namespace Task\Test\Model\Category;

class DataProvider extends \Magento\Catalog\Model\Category\DataProvider
{
    public function getData()
    {
        $data = parent::getData();

        foreach ($data as $key=>$value) {
            if (isset($data[$key]['category_image_desktop'])) {
                unset($data[$key]['category_image_desktop']);
            }
            if (isset($data[$key]['category_image_mobile'])) {
                unset($data[$key]['category_image_mobile']);
            }
        }

        return $data;
    }

    protected function getFieldsMap()
    {
        $fields = parent::getFieldsMap();
        $fields['content'][] = 'category_image_desktop';
        $fields['content'][] = 'category_image_mobile';

        return $fields;
    }
}