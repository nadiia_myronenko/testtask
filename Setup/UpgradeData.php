<?php

namespace Task\Test\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Model\Config;
use Magento\Eav\Setup\EavSetupFactory;

class UpgradeData implements UpgradeDataInterface
{
    private $eavSetupFactory;
    private $eavConfig;

    public function __construct(EavSetupFactory $eavSetupFactory, Config $eavConfig)
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->eavConfig = $eavConfig;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.12', '<')) {

            $categoryImageDesktopAttribute = $this->eavSetupFactory->create();
            $categoryImageDesktopAttribute->addAttribute(
                \Magento\Catalog\Model\Category::ENTITY,
                'category_image_desktop',
                [
                    'type' => 'varchar',
                    'label' => 'Category Image Desktop Attribute',
                    'input' => 'image',
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'backend' => 'Task\Test\Model\Category\Attribute\Backend\Thumb',
                    'visible' => true,
                    'required' => false,
                    'user_defined' => false,
                    'default' => null,
                    'group' => 'general',
                ]
            );

            $categoryImageMobileAttribute = $this->eavSetupFactory->create();
            $categoryImageMobileAttribute->addAttribute(
                \Magento\Catalog\Model\Category::ENTITY,
                'category_image_mobile',
                [
                    'type' => 'varchar',
                    'label' => 'Category Image Mobile Attribute',
                    'input' => 'image',
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'backend' => 'Task\Test\Model\Category\Attribute\Backend\Thumb',
                    'visible' => true,
                    'required' => false,
                    'user_defined' => false,
                    'default' => null,
                    'group' => 'general',
                ]
            );
        }

        $setup->endSetup();
    }
}